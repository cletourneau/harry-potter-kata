package org.lunatik.kata.harrypotter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HarryPotterBookPriceCalculator {
    
    public static final float PRICE_FOR_ONE_BOOK = 8.0f;
    private static final Map<Integer, Double> DISCOUNTS = new HashMap<Integer, Double>(5) {
        {
            put(1, 0.0d);
            put(2, 0.05d);
            put(3, 0.1d);
            put(4, 0.2d);
            put(5, 0.25d);
        }
    };

    public double priceFor(Integer... books) {
        List<Integer> bookList = convertBooksInList(books);
        List<Set<Integer>> setsOfDifferentBooks = extractAllSetsOfDifferentBooks(bookList);

        double totalPrice = 0.0d;
        for (Set<Integer> aSetOfDifferentBooks : setsOfDifferentBooks) {
            totalPrice += getPriceForASetOfDifferentBook(aSetOfDifferentBooks.size());
        }
        return totalPrice;
    }

    private List<Integer> convertBooksInList(final Integer[] books) {
        return new ArrayList<Integer>(Arrays.asList(books));
    }

    private double getPriceForASetOfDifferentBook(final Integer differentBookCount) {
        double discount = DISCOUNTS.get(differentBookCount);
        double totalPriceWithoutDiscount = differentBookCount * PRICE_FOR_ONE_BOOK;
        return totalPriceWithoutDiscount - (totalPriceWithoutDiscount * discount);
    }

    private List<Set<Integer>> extractAllSetsOfDifferentBooks(List<Integer> allBooks) {
        List<Integer> remainingBooks = new ArrayList<Integer>(allBooks);
        List<Set<Integer>> setsOfDifferentBooks = new ArrayList<Set<Integer>>(10);

        while (remainingBooks.size() > 0) {
            final Set<Integer> oneSetOfDifferentBooks = extractNextSetOfDifferentBook(remainingBooks);
            setsOfDifferentBooks.add(oneSetOfDifferentBooks);
            removeCountedBooks(oneSetOfDifferentBooks, remainingBooks);
        }

        return setsOfDifferentBooks;
    }

    private Set<Integer> extractNextSetOfDifferentBook(List<Integer> books) {
        Set<Integer> differentBooks = new HashSet<Integer>(5);
        differentBooks.addAll(books);
        return differentBooks;
    }

    private void removeCountedBooks(final Set<Integer> countedBooks, final List<Integer> remainingBooks) {
        for (Integer countedBook : countedBooks) {
            remainingBooks.remove(countedBook);
        }
    }
}
