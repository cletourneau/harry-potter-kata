package org.lunatik.kata.harrypotter2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Calculator {
    public static final float PRICE_FOR_ONE_BOOK = 8.0f;
    private List<Set<Integer>> setsOfBooks = new ArrayList<Set<Integer>>(10);

    private static final Map<Integer, Double> DISCOUNTS = new HashMap<Integer, Double>() {
        {
            put(1, 0.0d);
            put(2, 0.05d);
            put(3, 0.10d);
            put(4, 0.20d);
            put(5, 0.25d);
        }
    };

    public double priceFor(Integer... books) {
        setsOfBooks.clear();
        extractOptimalSetOfBooks(books);
        return calculatePriceForAllSetsOfBooks();
    }

    private double calculatePriceForAllSetsOfBooks() {
        double totalPrice = 0.0d;
        for (Set<Integer> setOfBooks : setsOfBooks) {
            totalPrice += priceForASetOfBooks(setOfBooks.size());
        }
        return totalPrice;
    }

    private void extractOptimalSetOfBooks(Integer[] books) {
        for (Integer book : books) {
            if (!weAddedABookToAnExistingSet(book)) {
                createANewSetOfBooksWith(book);
            }
            sortSetsForOptimalPricing();
        }
    }

    private void sortSetsForOptimalPricing() {
        Collections.sort(setsOfBooks, new Comparator<Set<Integer>>() {
            @Override
            public int compare(Set<Integer> firstSetOfBooks, Set<Integer> secondSetOfBooks) {
                if (canGreatlyOptimizeSetWithOneMoreBook(firstSetOfBooks)) return -1;
                if (canGreatlyOptimizeSetWithOneMoreBook(secondSetOfBooks)) return 1;
                return ((Integer) secondSetOfBooks.size()).compareTo(firstSetOfBooks.size());
            }
        });
    }

    private boolean canGreatlyOptimizeSetWithOneMoreBook(final Set<Integer> setOfBooks) {
        return setOfBooks.size() == 3;
    }

    private boolean weAddedABookToAnExistingSet(Integer book) {
        for (Set<Integer> aSetOfBooks : setsOfBooks) {
            if (!aSetOfBooks.contains(book)) {
                aSetOfBooks.add(book);
                return true;
            }
        }
        return false;
    }

    private void createANewSetOfBooksWith(final Integer book) {
        setsOfBooks.add(new HashSet<Integer>(5) {
            {
                add(book);
            }
        });
    }

    private double priceForASetOfBooks(int bookCount) {
        double discount = DISCOUNTS.get(bookCount); 
        double priceBeforeDiscount = bookCount * PRICE_FOR_ONE_BOOK;
        return priceBeforeDiscount - (priceBeforeDiscount * discount);
    }
}
