package org.lunatik.kata.harrypotter;

import static org.junit.Assert.*;
import static org.lunatik.kata.harrypotter.HarryPotterBookPriceCalculator.PRICE_FOR_ONE_BOOK;
import static org.lunatik.kata.util.IsCloseTo.isCloseTo;

import org.junit.Ignore;
import org.junit.Test;

public class HarryPotterBookPriceCalculatorTest {

    private static final int PHILOSOPHERS_STONE = 0;
    private static final int CHAMBER_OF_SECRETS = 1;
    private static final int PRISONER_OF_AZKABAN = 2;
    private static final int GOBLET_OF_FIRE = 3;
    private static final int ORDER_OF_THE_PHOENIX = 4;

    @Test public void
    noBookShouldCostNothing() {
        assertThat(thePriceFor(), isCloseTo(0.0d));
    }

    @Test public void
    oneBookShouldNotHaveAnyDiscountApplied() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE), isCloseTo(aOneBookSetPrice()));
        assertThat(thePriceFor(CHAMBER_OF_SECRETS), isCloseTo(aOneBookSetPrice()));
        assertThat(thePriceFor(PRISONER_OF_AZKABAN), isCloseTo(aOneBookSetPrice()));
        assertThat(thePriceFor(GOBLET_OF_FIRE), isCloseTo(aOneBookSetPrice()));
        assertThat(thePriceFor(ORDER_OF_THE_PHOENIX), isCloseTo(aOneBookSetPrice()));
    }

    @Test public void
    manySimilarBooksShouldNotHaveAnyDiscountApplied() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, PHILOSOPHERS_STONE), isCloseTo(2 * aOneBookSetPrice()));
        assertThat(thePriceFor(CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS), isCloseTo(3 * aOneBookSetPrice()));
        assertThat(thePriceFor(ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX), isCloseTo(4 * aOneBookSetPrice()));
    }

    @Test public void
    twoDifferentBooksShouldHaveAFivePercentDiscountApplied() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS), isCloseTo(aTwoBookSetPrice()));
    }

    @Test public void
    threeDifferentBooksShouldHaveATenPercentDiscountApplied() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS, PRISONER_OF_AZKABAN), isCloseTo(aThreeBookSetPrice()));
    }

    @Test public void
    fourDifferentBooksShouldHaveATwentyPercentDiscountApplied() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS, PRISONER_OF_AZKABAN, GOBLET_OF_FIRE), isCloseTo(aFourBookSetPrice()));
    }

    @Test public void
    fiveDifferentBooksShouldHaveATwentyFivePercentDiscountApplied() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS, PRISONER_OF_AZKABAN, GOBLET_OF_FIRE, ORDER_OF_THE_PHOENIX), isCloseTo(aFiveBookSetPrice()));
    }

    @Test public void
    fourBooksButOnlyThreeDifferentShouldHaveATenPercentDiscountAppliedOnThreeBooks() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, GOBLET_OF_FIRE, ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX), isCloseTo(aThreeBookSetPrice() + aOneBookSetPrice()));
    }

    @Test public void
    sixBooksWithTwoSetsOfThreeDifferentBooksShouldHaveADiscountOfTenPercent() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS, ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX), isCloseTo((aThreeBookSetPrice()) + (aThreeBookSetPrice())));
    }

    @Test public void
    testSeveralSimpleDiscounts() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS), isCloseTo(aOneBookSetPrice() + aTwoBookSetPrice()));
        assertThat(thePriceFor(PHILOSOPHERS_STONE, PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS), isCloseTo(aTwoBookSetPrice() + aTwoBookSetPrice()));
        assertThat(thePriceFor(PHILOSOPHERS_STONE, PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS, PRISONER_OF_AZKABAN, PRISONER_OF_AZKABAN, GOBLET_OF_FIRE), isCloseTo(aFourBookSetPrice() + aTwoBookSetPrice()));
        assertThat(thePriceFor(PHILOSOPHERS_STONE, CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS, PRISONER_OF_AZKABAN, GOBLET_OF_FIRE, ORDER_OF_THE_PHOENIX), isCloseTo(aOneBookSetPrice() + aFiveBookSetPrice()));
    }
    
    @Test @Ignore public void
    testEdgeCases() {
        assertThat(thePriceFor(PHILOSOPHERS_STONE, PHILOSOPHERS_STONE,
                CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS,
                PRISONER_OF_AZKABAN, PRISONER_OF_AZKABAN,
                GOBLET_OF_FIRE,
                ORDER_OF_THE_PHOENIX), isCloseTo(aFourBookSetPrice() + aFourBookSetPrice()));
        assertThat(thePriceFor(PHILOSOPHERS_STONE, PHILOSOPHERS_STONE, PHILOSOPHERS_STONE, PHILOSOPHERS_STONE, PHILOSOPHERS_STONE,
                CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS, CHAMBER_OF_SECRETS,
                PRISONER_OF_AZKABAN, PRISONER_OF_AZKABAN, PRISONER_OF_AZKABAN, PRISONER_OF_AZKABAN,
                GOBLET_OF_FIRE, GOBLET_OF_FIRE, GOBLET_OF_FIRE, GOBLET_OF_FIRE, GOBLET_OF_FIRE,
                ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX, ORDER_OF_THE_PHOENIX), isCloseTo(3 * aFiveBookSetPrice() + 2 * aFourBookSetPrice()));
    }

    private static float aOneBookSetPrice() {
        return 1 * PRICE_FOR_ONE_BOOK;
    }

    private double aTwoBookSetPrice() {
        return 2 * PRICE_FOR_ONE_BOOK * 0.95d;
    }

    private double aThreeBookSetPrice() {
        return 3 * PRICE_FOR_ONE_BOOK * 0.90d;
    }

    private double aFourBookSetPrice() {
        return 4 * PRICE_FOR_ONE_BOOK * 0.80d;
    }

    private double aFiveBookSetPrice() {
        return 5 * PRICE_FOR_ONE_BOOK * 0.75d;
    }

    private double thePriceFor(Integer... books) {
        return new HarryPotterBookPriceCalculator().priceFor(books);
    }

}
