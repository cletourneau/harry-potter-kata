package org.lunatik.kata.util;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;


/**
 * Is the value a number equal to a value within some range of
 * acceptable error?
 */
public class IsCloseTo extends BaseMatcher<Double> {
    private final double delta;
    private final double value;

    public IsCloseTo(double value, double error) {
        this.delta = error;
        this.value = value;
    }

    @Override
    public boolean matches(Object item) {
        final Double value = (Double) item;
        return actualDelta(value) <= 0.0;
    }

    public void describeTo(Description description) {
        description.appendText("a numeric value within ")
                .appendValue(delta)
                .appendText(" of ")
                .appendValue(value);
    }

    private double actualDelta(Double item) {
        return (Math.abs((item - value)) - delta);
    }


    public static Matcher<Double> isCloseTo(double operand, double error) {
        return new IsCloseTo(operand, error);
    }

    public static Matcher<Double> isCloseTo(double operand) {
        return new IsCloseTo(operand, 0.001d);
    }

}